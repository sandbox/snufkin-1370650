<?php

/**
 * @file
 * This module provides some example testing code for FCC.
 */

/**
 * Implements hook_menu().
 */
function fcc_sandbox_menu() {
  $items['admin/config/fcc-sandbox'] = array(
    'title' => 'Configure FCC sandbox',
    'description' => 'FCC sandbox configuration',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('fcc_sandbox_admin_setting'),
    'access arguments' => array('administer fcc adverts'),
    'file' => 'fcc_sandbox.admin.inc',
  );
  $items['fcc-sandbox'] = array(
    'title' => 'FCC sandbox page',
    'page callback' => 'fcc_sandbox_example_page',
    'access arguments' => array('access content'),
  );
  $items['fcc-adverts/add'] = array(
    'title' => 'Create new FCC advert',
    'page callback' => 'fcc_sandbox_add_page',
    'access arguments' => array('administer fcc adverts'),
  );
  $items['fcc-adverts/%fcc_sandbox_advert/edit'] = array(
    'title' => 'Edit FCC advert',
    'page callback' => 'fcc_sandbox_edit_advert',
    'page arguments' => array(1),
    'access callback' => 'fcc_sandbox_access',
    'access arguments' => array('administer fcc adverts'),
  );
  $items['fcc-adverts/%fcc_sandbox_advert/delete'] = array(
    'title' => 'Delete FCC advert',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('fcc_sandbox_delete_form', 1),
    'access callback' => 'fcc_sandbox_access',
    'access arguments' => array('administer fcc adverts'),
  );
  $items['fcc-advert/ajax'] = array(
    'page callback' => 'fcc_sandbox_ajax_response',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['fcc-service'] = array(
    'page callback' => 'fcc_sandbox_service',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

function fcc_sandbox_service() {
  $url = 'http://api.offershq.com/1.2/offers.php';
  $data = array(
    'location' => 'fargo',
    'searchword' => 'bars',
  );

  $full_url = url($url, array('query' => $data));
  $response = drupal_http_request($full_url);

  dpm($response);
  if ($response->code != 200) {
    watchdog('fcc_sandbox', 'Unable to access remote server %server', array('%server' => $url), WATCHDOG_ERROR);
  }
  else {
    $data = drupal_json_decode($response->data);
    dpm($data);
  }


  $build = array();
  return $build;
}

function fcc_sandbox_ajax_response($aid) {
  $advert = fcc_sandbox_advert_load($aid);
  $rendered_advert = theme('fcc_advert', array('advert' => $advert));
  $commands = array();
  $commands[] = ajax_command_replace('#advertDiv', $rendered_advert);
  $build = array('#type' => 'ajax', '#commands' => $commands);
  ajax_deliver($build);
}

function fcc_sandbox_access($perm) {
  global $user;

  if (1 == $user->uid) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

function fcc_sandbox_delete_advert($advert) {
  return drupal_get_form('fcc_sandbox_delete_form', $advert);
}

function fcc_sandbox_delete_form($form, &$form_state, $advert) {
  $form['aid'] = array(
    '#type' => 'value',
    '#value' => $advert->aid,
  );
  $form['#advert'] = $advert;
  return confirm_form($form,
    t('Are you sure?'),
    '',
    t('You are about to delete advert @description', array('@description' => $advert->description)),
    t('Delete'),
    t('Cancel'),
    'fcc_sandbox_delete_form');
}

function fcc_sandbox_delete_form_submit($form, &$form_state) {

  db_delete('fcc_sandbox_ads')->condition('aid', $form['#advert']->aid)->execute();

  drupal_set_message(t('@description has been deleted.', array('@description' => $form['#advert']->description)));
  $form_state['redirect'] = '<front>';
}

function fcc_sandbox_advert_load($ad_id) {
  return db_query("SELECT * FROM {fcc_sandbox_ads} WHERE aid = :ad_id", array(':ad_id' => $ad_id))->fetchObject();
}

function fcc_sandbox_edit_advert($advert) {
  return drupal_get_form('fcc_sandbox_advert_form', $advert);
}

/**
 * Implements hook_permission().
 */
function fcc_sandbox_permission() {
  return array(
    'administer fcc adverts' => array(
      'title' => t('Administer FCC Adverts'),
      'description' => t('Perform administrative tasks on FCC adverts.'),
    ),
  );
}

function fcc_sandbox_example_page() {
  $output = t('Load an advert by clicking on the link below.');
  $build['explanation'] = array(
    '#markup' => $output,
  );

  $build['ajax_link'] = array(
    '#type' => 'link',
    '#title' => t('Load advert'),
    '#href' => 'fcc-advert/ajax/4',
    '#id' => 'ajax_link',
    '#ajax' => array(
      'wrapper' => 'advertDiv',
      'method' => 'html',
    ),
  );

  $build['advert'] = array(
    '#markup' => '<div id="advertDiv"></div>',
  );
  $advert = fcc_sandbox_advert_load(4);

  $build['advert_second'] = array(
    '#theme' => 'fcc_advert',
    '#advert' => $advert,
    '#attachment' => array()
  );


  drupal_add_js(array('fcc_sandbox' => array('aid' => $advert->aid, 'advert' => $advert)), 'setting');
  $js = 'jQuery(document).ready(
    function () { 
      console.log(Drupal.settings.fcc_sandbox);
    }
  );';
  drupal_add_js($js, array('type' => 'inline', 'scope' => 'footer', 'weight' => 5));
  return $build;
}

function fcc_sandbox_add_page() {
  $build['advert_form'] = drupal_get_form('fcc_sandbox_advert_form');

  $list = array(
    'first item',
    'second item',
    'third item',
  );

  $build['list'] = array(
    '#theme' => 'item_list',
    '#items' => $list,
  );
  return $build;
}

function fcc_sandbox_advert_form($form, $form_state, $advert = NULL) {
  $form = array();

  // If the advert is being edited.
  if (isset($advert->aid)) {
    $form['aid'] = array(
      '#type' => 'hidden',
      '#value' => $advert->aid,
    );
  }

  $form['content'] = array(
    '#title' => t('Content'),
    '#type' => 'textarea',
    '#description' => t('Content of the advert'),
    '#default_value' => isset($advert->content) ? $advert->content : '',
  );
  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#description' => t('Advert title'),
    '#default_value' => isset($advert->description) ? $advert->description : '',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save advert'),
  );
  return $form;
}

function fcc_sandbox_advert_form_validate($form, &$form_state) {
  if (drupal_strlen($form_state['values']['description']) > 128) {
    form_set_error('description', t('Description can not be longer than 128 characters.'));
  }
}

function fcc_sandbox_advert_form_submit($form, &$form_state) {
  if (isset($form_state['values']['aid'])) {
    drupal_set_message(t('Advert %description saved',
      array('%description' => $form_state['values']['description'])));
  }
  else {
    drupal_set_message(t('New advert saved'));
  }
  $aid = isset($form_state['values']['aid']) ? $form_state['values']['aid'] : NULL;
  db_merge('fcc_sandbox_ads')
    ->key(array('aid' => $aid))
    ->fields(array(
      'content' => $form_state['values']['content'],
      'description' => $form_state['values']['description'],
      'created' => time(),
    ))
    ->execute();
}

/**
 * Implements hook_form_alter().
 */
function fcc_sandbox_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'comment_node_article_form') {
    $form['subject']['#type'] = 'textarea';
  }
}

/**
 * Implements hook_form_FROM_ID_alter().
 */
function fcc_sandbox_form_comment_node_article_form_alter(&$form, $form_state) {
  $form['subject']['#title'] = t('Awesome subject');
}

/**
 * Implements hook_node_view().
 */
function fcc_sandbox_node_view_alter(&$build) {
//  dpm($build);
//  unset($build['field_teaser']);
//  hide($build['field_teaser']);
//  dpm($build);
}

/**
 * Implements hook_theme().
 */
function fcc_sandbox_theme() {
  return array(
    'fcc_advert' => array(
      'template' => 'fcc-advert',
      'variables' => array(
        'advert' => NULL,
      ),
    ),
  );
}

function fcc_sandbox_preprocess_fcc_advert(&$variables) {
}

function fcc_sandbox_preprocess__fcc_advert(&$variables) {
//  dpm($variables);
}

function template_preprocess_fcc_advert(&$variables) {
  $advert = $variables['advert'];
  $variables['description_teaser'] = truncate_utf8($advert->description, 40, TRUE, TRUE);
  $variables['content'] = $advert->content;
  $variables['description'] = $advert->description;
}

/**
 * Theming fcc_advert.
 */
function theme_fcc_advert(&$variables) {
  dpm('hello?');
  $output = '<h3>' . check_plain($variables['description_teaser']) . '</h3>';
  $output .= '<p>' . check_plain($variables['description']) . '</p>';
  $output .= '<p>' . check_plain($variables['content']) . '</p>';
  return $output;
}
