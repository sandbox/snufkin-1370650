<div class="fcc-advert">
  <div class="description-teaser"><?php print $description_teaser; ?></div>
  <div class="content"><?php print $content; ?></div>
  <div class="description"><?php print $description; ?></div>
</div>

<?php if ($logged_in) : ?>
<hr />
<?php endif; ?>