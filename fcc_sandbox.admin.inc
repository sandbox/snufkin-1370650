<?php

/**
 * @file
 * Administrative callbacks for FCC Sandbox.
 */

function fcc_sandbox_admin_setting() {
  $form = array();
  $options = array(
    0 => t('No advert'),
    1 => t('One advert'),
    5 => t('Five advert'),
    100000 => t('Too many adverts'),
  );
  $form['number_of_frontpage_ads'] = array(
    '#type' => 'select',
    '#title' => t('Number of frontpage ads'),
    '#description' => t('Select how many ads are to be displayed on the frontpage'),
    '#options' => $options,
    '#default_value' => variable_get('number_of_frontpage_ads', 0),
  );
  return system_settings_form($form);
}
